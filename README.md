# GitLab Token Import Plugin

This is a prototype plugin to demonstrate how we will use the Variables APIs to import design tokens in a Figma plugin. This sample imports Variables formatted using the [W3C Design Tokens spec](https://design-tokens.github.io/community-group/format/#design-token).

## Usage

Requirements:

1. [Node & NPM](https://nodejs.org/en/download/package-manager)
1. [Figma desktop app](https://www.figma.com/downloads/)

Building the Plugin:

1. Clone this repo to your local machine
1. Open locally cloned repo in terminal
1. `npm i`
1. `npm run dev`
1. The plugin is now built and will watch for any code changes.

Importing plugin to Figma:

1. Open Figma desktop app
1. `Plugins > Development > Import plugin from manifest`
1. Find and select the `manifest.json` file from your locally cloned repo.
1. `Plugins > Development > GitLab Variables > Import Variables`

To enable hot reloading (refreshes plugin if code changes are made locally) in Figma:

1. `Plugins > Development > Hot reload plugin`

_Note: this feature does not always have an active/inactive state in the UI, but it does work. Try selecting it again if you aren't seeing hot reloading._

## Troubleshooting

> An error occurred while loading the plugin environment

Make sure Figma is able to access the plugin files. On macOS see [control access to files and folders on Mac](https://support.apple.com/en-gb/guide/mac-help/mchld5a35146/mac).

## Test Cases

Here is a non-exhaustive list of test cases to ensure the plugin is working as expected:

- Import tokens into a fresh file
  - Does it create the correct collections?
  - Does it create the correct modes for each collection?
- Import tokens into a file that already has one/many of the collections
  - Does it retain collection IDs?
  - Does it retain existing variable IDs?
- Alter an existing variable value in one/many modes
  - Does it update the value to match the JSON SoT?
  - Does it retain the original variable ID?
  - Do variables that alias to this variable retain their link?
- Do variables with an alias in one mode and a value in another function as expected?
- Delete a variable that other variables alias to
  - Does the plugin re-create that variable?
  - Do the aliases regain their link to the re-created variable?
