interface Context {
  flattenedTokenStructure: boolean;
}

const context: Context = {
  flattenedTokenStructure: true,
};

export const getContext = () => context;
