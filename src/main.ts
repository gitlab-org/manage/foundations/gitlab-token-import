import { generateVariables } from './generateVariables';
import { transformTokens } from './transformTokens';
import { loadCollections } from './loadCollections';
import { generateTable } from './generateTable';

console.clear();

figma.ui.onmessage = async (e) => {
  try {
    if (e.type === 'IMPORT') {
      const { collectionId, data, flattened } = e;

      const localCollections =
        await figma.variables.getLocalVariableCollectionsAsync();
      const collection = localCollections.find((c) => c.id === collectionId);
      if (collection) {
        console.log('Collection exists: ', collection.name, collection.id);
      } else {
        throw new Error('Selected collection not found');
      }

      const tokenModes = new Set();
      const transformedTokens = transformTokens(
        data,
        flattened,
        collection.modes.map((mode) => mode.name)
      );

      // get mode names from upload data
      transformedTokens.forEach((token) => {
        if (token.$value instanceof Object) {
          Object.keys(token.$value).forEach((key) => {
            tokenModes.add(key);
          });
        }
      });

      // then compare mode names from upload data with existing modes in collection
      const mismatchedModes = Array.from(tokenModes).filter(
        (mode) => !collection.modes.find((m) => m.name === mode)
      );

      // if there are mismatched modes, everyone panic!!1!
      if (mismatchedModes.length > 0) {
        const msg =
          'Mode names mismatch. Please update collection so it contains the following modes: ' +
          mismatchedModes.join(', ');

        // nah, just kidding, we'll just surface the issue to the UI
        figma.notify(msg, { error: true, timeout: 7000 });

        // and throw an error to console
        throw new Error(msg);
      }

      // here on out we can safely assume that modes in the upload data and collection match
      const modes = collection.modes.map((m) => ({
        name: m.name,
        modeId: m.modeId,
      }));

      // TODO: we may be able to scope this down,
      // we only need variables that are aliased to/from in the incoming tokens
      const localVariables = await figma.variables.getLocalVariablesAsync();

      const results = await generateVariables(
        transformedTokens,
        modes,
        collection,
        localVariables
      );
      figma.ui.postMessage(results);
    } else if (e.type === 'GENERATE_TABLE') {
      console.log("Received GENERATE_TABLE message");
      await generateTable();
      console.log("Table generation complete, sending TABLE_GENERATED message");
      figma.ui.postMessage({ type: 'TABLE_GENERATED', message: 'Table generated successfully' });
    }
  } catch (error) {
    console.error('Error in Figma Plugin:', error);
    figma.notify('An error occurred. Check the console for details.', { error: true });
    figma.ui.postMessage({ type: 'ERROR', message: 'An error occurred' });
  }
};

if (figma.command === 'import') {
  figma.showUI(__uiFiles__['import'], {
    width: 600,
    height: 600,
    themeColors: true,
  });
  loadCollections();
}
