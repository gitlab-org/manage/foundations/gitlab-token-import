import { Extensions, getExtension } from './extensions';

type TSFix = any;

const LOCKED_PREFIX = '🔒/';
const DEPRECATED_PREFIX = '⚠️ DEPRECATED/';

const VALUE_KEY = '$value';
const DESCRIPTION_KEY = '$description';
const TYPE_KEY = '$type';
const EXTENSIONS_KEY = '$extensions';
const NACHT_KEY = 'nacht';
const DARK_KEY = 'dark';

function getGroupPrefix(item: TSFix) {
  if (getExtension(item, Extensions.GitLabLocked)) return LOCKED_PREFIX;
  else if (getExtension(item, Extensions.GitLabDeprecated))
    return DEPRECATED_PREFIX;
  else return '';
}

function mergeExtensions(parentExtensions: TSFix, childExtensions: TSFix) {
  return { ...parentExtensions, ...childExtensions };
}

function transformValue(value: TSFix) {
  if (typeof value === 'object' && value !== null) {
    if (value.hasOwnProperty(NACHT_KEY)) {
      if (value.hasOwnProperty(DARK_KEY)) {
        value[DARK_KEY] = value[NACHT_KEY];
      } else {
        value[DARK_KEY] = value[NACHT_KEY];
      }
      delete value[NACHT_KEY];
    }
  }
  return value;
}

export function transformTokens(
  input: TSFix,
  flattened: boolean,
  modes: string[]
): TSFix[] {
  const output: TSFix[] = [];

  function recurse(obj: TSFix, path: string[], parentExtensions: TSFix = {}) {
    for (const key in obj) {
      if (obj.hasOwnProperty(key) && typeof obj[key] === 'object') {
        const currentExtensions = mergeExtensions(
          parentExtensions,
          obj[key][EXTENSIONS_KEY] || {}
        );

        if (obj[key].hasOwnProperty(VALUE_KEY)) {
          const group = getGroupPrefix({ [EXTENSIONS_KEY]: currentExtensions });
          const separator = flattened ? '-' : '/';
          const name = `${group}${path.join(separator)}${separator}${key}`;

          let value = transformValue(obj[key][VALUE_KEY]);

          // Check if VALUE_KEY is not an object (single mode value) and multiple modes exist in the collection
          if (typeof value !== 'object' && modes.length > 0) {
            const modeValues: TSFix = {};
            modes.forEach((mode) => {
              modeValues[mode] = value;
            });
            value = modeValues;
          }

          const item = {
            name: name,
            [DESCRIPTION_KEY]: obj[key][DESCRIPTION_KEY],
            [VALUE_KEY]: value,
            [TYPE_KEY]: obj[key][TYPE_KEY],
            [EXTENSIONS_KEY]: currentExtensions,
          };
          output.push(item);
        } else {
          recurse(obj[key], [...path, key], currentExtensions);
        }
      }
    }
  }

  for (const category in input) {
    if (input.hasOwnProperty(category) && typeof input[category] === 'object') {
      const extensions = input[category][EXTENSIONS_KEY] || {};
      recurse(input[category], [category], extensions);
    }
  }

  return output;
}
