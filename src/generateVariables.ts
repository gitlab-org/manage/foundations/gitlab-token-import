import { Extensions, getExtension } from './extensions';

type TSFix = any; // TODO: Fix types

type Mode = { name: string; modeId: string };

type TokenCommon = {
  name: string;
  $type: string;
  $description?: string;
  $extensions?: TSFix;
};

type ConstantToken = TokenCommon & {
  $value: string;
};

type SemanticToken = TokenCommon & {
  $value: {
    [key: string]: string;
  };
};

type TokenGroup = (ConstantToken | SemanticToken)[];

const results = {
  created: 0,
  updated: 0,
  skipped: 0,
  invalid: 0,
};

function getCurrentResults() {
  const currentResults = { ...results };
  results.created = 0;
  results.updated = 0;
  results.skipped = 0;
  results.invalid = 0;
  return currentResults;
}

// Function to parse a color string and return an RGBA object
const parseColor = (value: string): RGBA => {
  const color = value.trim();
  const hexRegex = /^#([A-Fa-f0-9]{6})([A-Fa-f0-9]{2}){0,1}$/;
  const hexShorthandRegex = /^#([A-Fa-f0-9]{3})([A-Fa-f0-9]){0,1}$/;
  const rgbaRegex = /^rgba\(\s*(\d+),\s*(\d+),\s*(\d+),\s*([\d.]+)\s*\)$/;

  if (color.toLowerCase() === 'transparent') {
    return { r: 0, g: 0, b: 0, a: 0 };
  }

  if (hexRegex.test(color) || hexShorthandRegex.test(color)) {
    const hexValue = color.substring(1);
    const expandedHex =
      hexValue.length === 3 || hexValue.length === 4
        ? hexValue
            .split('')
            .map((char) => char + char)
            .join('')
        : hexValue;

    const alphaValue =
      expandedHex.length === 8 ? expandedHex.slice(6, 8) : undefined;

    return {
      r: parseInt(expandedHex.slice(0, 2), 16) / 255,
      g: parseInt(expandedHex.slice(2, 4), 16) / 255,
      b: parseInt(expandedHex.slice(4, 6), 16) / 255,
      a: alphaValue ? parseInt(alphaValue, 16) / 255 : 1,
    };
  }
  if (rgbaRegex.test(color)) {
    const match = color.match(rgbaRegex);
    if (match) {
      const [, red, green, blue, alpha] = match;
      return {
        r: parseInt(red, 10) / 255,
        g: parseInt(green, 10) / 255,
        b: parseInt(blue, 10) / 255,
        a: parseFloat(alpha),
      };
    }
  }
  throw new Error('Invalid color format');
};

const rgbToHex = ({ r, g, b, a = 1 }: RGBA) => {
  const toHex = (value: number) => {
    const hex = Math.round(value * 255).toString(16);
    return hex.length === 1 ? `0${hex}` : hex;
  };

  const hex = [toHex(r), toHex(g), toHex(b)].join('');
  return `#${hex}${a !== 1 ? toHex(a) : ''}`;
};

const colorApproximatelyEqual = (a: RGBA, b: RGBA) => {
  return rgbToHex(a) === rgbToHex(b);
};

// Function to check if a value is an alias
const isAlias = (value: string): boolean => {
  return value.trim().charAt(0) === '{';
};

// Function to traverse a collection and extract tokens
const traverseCollection = ({
  key,
  object,
  tokens,
}: {
  key: string;
  object: TSFix;
  tokens: TSFix[];
}) => {
  if (key.charAt(0) === '$') {
    return;
  }

  if (object.$value !== undefined) {
    tokens[Number(key)] = { name: key, ...object } as TSFix;
  } else {
    const entries = Object.keys(object).map((key) => [key, object[key]]);
    entries.forEach(([key2, object2]) => {
      if (key2.charAt(0) !== '$' && typeof object2 === 'object') {
        traverseCollection({
          key: `${key}/${key2}`,
          object: { name: `${key}/${key2}`, ...object2 },
          tokens,
        });
      }
    });
  }
};

async function createOrUpdateToken(
  collection: VariableCollection,
  modeId: string,
  type: VariableResolvedDataType,
  name: string,
  value: VariableValue,
  description?: string,
  scopes?: VariableScope[]
): Promise<Variable | null> {
  try {
    const localVariables = await figma.variables.getLocalVariablesAsync();
    const existingVariable = localVariables.find(
      (v) => v.name === name && v.variableCollectionId === collection.id
    );

    if (existingVariable) {
      const currentValue = existingVariable.valuesByMode[modeId];

      if (colorApproximatelyEqual(currentValue as RGBA, value as RGBA)) {
        results.skipped++;
      } else {
        results.updated++;
        await setVariableModeValue(existingVariable, modeId, value);
      }
      if (scopes && scopes.length > 0) {
        existingVariable.scopes = [...scopes];
      }
      if (description && description.trim() !== '') {
        existingVariable.description = description;
      }
      return existingVariable;
    }

    const newVariable = await figma.variables.createVariable(
      name,
      collection,
      type
    );

    if (newVariable) {
      await setVariableModeValue(newVariable, modeId, value);
      if (description && description.trim() !== '') {
        newVariable.description = description;
      }
      if (scopes && scopes.length > 0) {
        newVariable.scopes = [...scopes];
      }
      results.created++;
    }
    return newVariable;
  } catch (error) {
    console.log('Error creating or updating variable: ', error);
    results.invalid++;
    return null;
  }
}

async function setVariableModeValue(
  variable: Variable,
  modeId: string,
  value: VariableValue
) {
  try {
    await variable.setValueForMode(modeId, value);
  } catch (error) {
    console.log('error: ', error);
  }
}

function tokenAliasValue(id: string) {
  const value: VariableAlias = {
    type: 'VARIABLE_ALIAS',
    id: id,
  };
  return value;
}

async function processAliases({
  collection,
  localModes,
  localVariables,
  aliases,
  tokens,
}: {
  collection: VariableCollection;
  localModes: TSFix;
  localVariables: Variable[];
  aliases: { [x: string]: any };
  tokens: { [x: string]: any };
}) {
  const aliasKeys = Object.keys(aliases);
  let generations = aliasKeys.length;
  const unresolvedAliases = new Set(aliasKeys);

  while (generations > 0 && unresolvedAliases.size > 0) {
    for (const aliasKey of Array.from(unresolvedAliases)) {
      // check for alias within collection
      const alias = aliases[aliasKey];
      const modeId = alias.modeId;
      const tokenName = alias.valueKey;
      const token = tokens[tokenName];

      if (token) {
        tokens[alias.key] = await createOrUpdateToken(
          collection,
          modeId,
          token.resolvedType,
          alias.key,
          tokenAliasValue(token.id),
          token?.$description,
          alias.scopes
        );
        unresolvedAliases.delete(aliasKey);
      } else {
        // check for alias within local variables
        for (const localVar of localVariables) {
          if (localVar.name === tokenName) {
            tokens[alias.key] = await createOrUpdateToken(
              collection,
              modeId,
              localVar.resolvedType,
              alias.key,
              tokenAliasValue(localVar.id),
              localVar?.description,
              alias.scopes
            );
            unresolvedAliases.delete(aliasKey);
            break;
          }
        }
      }
    }
    generations--;
  }

  if (unresolvedAliases.size > 0) {
    results.skipped = unresolvedAliases.size;
    console.log(
      '🚨 Unresolved aliases:',
      Array.from(unresolvedAliases).map((aliasKey) => {
        const alias = aliases[aliasKey];
        const modeName =
          localModes.find(
            (mode: { modeId: TSFix }) => mode.modeId === alias.modeId
          )?.name || 'Unknown mode';
        return {
          name: aliasKey.replace(/_\d+:\d+$/, ''), // removes mode suffix
          'attempted alias': alias?.valueKey,
          mode: modeName,
        };
      })
    );
  }
}

async function traverseToken({
  collection,
  modeId,
  type,
  key,
  object,
  tokens,
  aliases,
  scopes,
}: {
  collection: VariableCollection;
  modeId: string;
  type: string;
  key: string;
  object: any;
  tokens: { [x: string]: any };
  aliases: { [x: string]: any };
  scopes?: VariableScope[];
}) {
  type = type || object.$type;
  if (key.charAt(0) === '$') {
    return;
  }

  if (object.$value !== undefined) {
    if (isAlias(object.$value)) {
      const valueKey = object.$value
        .trim()
        .replace(/\./g, '-')
        .replace(/[\{\}]/g, '');
      const token = tokens[valueKey];

      if (token) {
        tokens[key] = await createOrUpdateToken(
          collection,
          modeId,
          token.resolvedType,
          key,
          tokenAliasValue(token.id),
          token?.$description,
          scopes
        );
      } else {
        aliases[`${key}_${modeId}`] = {
          key,
          type,
          modeId,
          valueKey,
          scopes,
        };
      }
    } else if (type === 'color') {
      tokens[key] = await createOrUpdateToken(
        collection,
        modeId,
        'COLOR',
        key,
        parseColor(object.$value),
        object.$description,
        scopes
      );
    } else if (type === 'number') {
      tokens[key] = await createOrUpdateToken(
        collection,
        modeId,
        'FLOAT',
        key,
        object.$value,
        object.$description,
        scopes
      );
    } else {
      console.log('unsupported type', type, object);
    }
  } else {
    const entries = Object.keys(object).map((key2) => [key2, object[key2]]);
    for (const [key2, object2] of entries) {
      if (key2.charAt(0) !== '$') {
        await traverseToken({
          collection,
          modeId,
          type,
          key: `${key}/${key2}`,
          object: object2,
          tokens,
          aliases,
          scopes,
        });
      }
    }
  }
}

const generateVariables = async (
  JSONData: TokenGroup,
  localModes: Mode[],
  collection: VariableCollection,
  localVariables: Variable[] = []
): Promise<TSFix> => {
  const rawTokens: {
    modeId: string;
    name: any;
    scopes?: VariableScope[];
    $description?: string;
    $value: any;
    $type: string;
  }[] = [];

  JSONData.forEach((token) => {
    const { name, $type, $description, $extensions } = token;
    const scopes: VariableScope[] = [];

    const scopeExtension = getExtension(token, Extensions.FigmaVariableScope);

    // This seems like it could be improved
    if (scopeExtension) {
      scopeExtension.forEach((scope: VariableScope) => {
        scopes.push(scope);
      });
    }

    if (typeof token.$value === 'string') {
      rawTokens.push({
        name,
        modeId: localModes[0].modeId,
        $description,
        $value: token.$value,
        $type,
        scopes,
      });
    } else {
      localModes.forEach((mode: Mode) => {
        let v;
        if (typeof token.$value === 'object') {
          v = token.$value[mode.name];
        } else {
          v = token.$value;
        }

        rawTokens.push({
          name,
          modeId: mode.modeId,
          $description,
          $value: v,
          $type,
          scopes,
        });
      });
    }
  });

  const tokens: { [x: string]: any } = {};
  const aliases: { [x: string]: any } = {};

  for (const token of rawTokens) {
    await traverseToken({
      collection,
      modeId: token.modeId,
      type: token.$type,
      key: token.name,
      object: token,
      tokens,
      aliases,
      scopes: token.scopes,
    });
  }

  await processAliases({
    collection,
    localModes,
    localVariables,
    aliases,
    tokens,
  });

  console.log('Results:', results);
  console.log('Tokens:', tokens);

  return getCurrentResults();
};

export { generateVariables };
