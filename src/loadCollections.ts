export async function loadCollections(id: string | null = null) {
  const localCollections =
    await figma.variables.getLocalVariableCollectionsAsync();

  let collections: { id: string; name: string }[] = [];

  localCollections.forEach((c) => {
    collections.push({
      id: c.id,
      name: c.name,
    });
  });

  figma.ui.postMessage({
    type: 'VARIABLE_COLLECTIONS',
    collections: collections,
    activeCollectionId: id || null,
  });
}
