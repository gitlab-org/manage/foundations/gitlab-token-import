export enum Extensions {
  FigmaVariableScope = 'com.figma.scope',
  GitLabDeprecated = 'com.gitlab.deprecated',
  GitLabLocked = 'com.gitlab.locked',
}

type ExtensionValues = {
  [Extensions.FigmaVariableScope]: VariableScope[];
  [Extensions.GitLabDeprecated]: boolean;
  [Extensions.GitLabLocked]: boolean;
};

type TokenJSON = { $extensions?: Partial<ExtensionValues> };

// TODO: is this helpful?
export function hasExtensions(token: TokenJSON): boolean {
  return !!token.$extensions;
}

// TODO: is this helpful?
export function getExtensions(
  token: TokenJSON
): Partial<ExtensionValues> | undefined {
  const extensionsExist = hasExtensions(token);

  if (!extensionsExist) {
    return undefined;
  }

  return token.$extensions;
}

export function getExtension<K extends keyof ExtensionValues>(
  token: TokenJSON,
  key: K
): ExtensionValues[K] | undefined {
  const extensions = getExtensions(token);

  return extensions?.[key];
}
