export async function generateTable() {
  // Load all pages and fonts
  await figma.loadAllPagesAsync();
  await Promise.all([
    figma.loadFontAsync({ family: "GitLab Mono", style: "Regular" }),
    figma.loadFontAsync({ family: "GitLab Sans", style: "Regular" })
  ]);

  // Helper function to pad a string
  function padStart(str: string, targetLength: number, padString: string) {
    targetLength = targetLength >> 0; //truncate if number or convert non-number to 0;
    padString = String((typeof padString !== 'undefined' ? padString : ' '));
    if (str.length > targetLength) {
      return String(str);
    } else {
      targetLength = targetLength - str.length;
      if (targetLength > padString.length) {
        padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
      }
      return padString.slice(0, targetLength) + String(str);
    }
  }

  // Helper function to convert color to hex
  function rgbToHex(r: number, g: number, b: number, a = 1) {
    const toHex = (value: number) => padStart(Math.round(value * 255).toString(16), 2, '0');
    const hex = `#${toHex(r)}${toHex(g)}${toHex(b)}`;
    return a < 1 ? `${hex}${toHex(a)}` : hex;
  }

  // Get variables from the correct collection
  const collections = await figma.variables.getLocalVariableCollectionsAsync();
  const collection = collections.find(c => c.name === "Tokens: Color");
  if (!collection) {
    throw new Error("Variable collection 'Tokens: Color' not found!");
  }
  
  const variables = await figma.variables.getLocalVariablesAsync();
  const colorVars = variables.filter(v => v.variableCollectionId === collection.id);

  // Sort variables to match the order in the variables window
  colorVars.sort((a, b) => {
    const aIndex = collection.variableIds.indexOf(a.id);
    const bIndex = collection.variableIds.indexOf(b.id);
    return aIndex - bIndex;
  });

  // Filter out variables in specific groups
  const filteredColorVars = colorVars.filter(v => !v.name.startsWith('🔒') && !v.name.startsWith('⚠️ DEPRECATED'));

  // Find the Design Tokens frame
  const tokenFrame = figma.currentPage.findChild(n => n.name === "Design tokens") as FrameNode;
  if (!tokenFrame) {
    throw new Error("Design tokens frame not found!");
  }

  // Find the row component
  const rowComponent = figma.root.findOne(n => n.name === "row" && n.type === "COMPONENT") as ComponentNode;
  if (!rowComponent) {
    throw new Error("Row component not found!");
  }

  // Remove existing rows (except the header)
  tokenFrame.children.slice(1).forEach(child => child.remove());

  // Create new rows
  for (const v of filteredColorVars) {
    const fullVariable = await figma.variables.getVariableByIdAsync(v.id);
    if (!fullVariable) continue;

    const rowInstance = rowComponent.createInstance();
    
    // Find col A, col B, and col C
    const colA = rowInstance.findOne(n => n.name === "col A") as FrameNode;
    const colB = rowInstance.findOne(n => n.name === "col B") as FrameNode;
    const colC = rowInstance.findOne(n => n.name === "col C") as FrameNode;
    
    // Update text layers
    const nameLayer = colA?.findOne(n => n.name === "Name") as TextNode;
    const descriptionLayerA = colA?.findOne(n => n.name === "Description") as TextNode;
    const descriptionLayerB = colB?.findOne(n => n.name === "Description") as TextNode;
    const valueLayer = colC?.findOne(n => n.name === "Value") as TextNode;
    const swatchLayer = colC?.findOne(n => n.name === "Swatch") as RectangleNode;
    
    if (nameLayer && fullVariable.name) nameLayer.characters = fullVariable.name;
    if (descriptionLayerA) descriptionLayerA.characters = fullVariable.description || '';
    if (descriptionLayerB) {
      const scopes = fullVariable.scopes || [];
      descriptionLayerB.characters = scopes.join(', ');
    }

    const value = fullVariable.valuesByMode[collection.defaultModeId];
    if (value !== undefined && value !== null) {
      if (typeof value === 'object' && 'type' in value && value.type === 'VARIABLE_ALIAS') {
        const aliasedVar = await figma.variables.getVariableByIdAsync(value.id);
        if (valueLayer) valueLayer.characters = aliasedVar ? aliasedVar.name : 'Unknown Variable';
      } else if (typeof value === 'object' && 'r' in value && 'g' in value && 'b' in value) {
        if (valueLayer) {
          const r = value.r, g = value.g, b = value.b;
          const a = 'a' in value ? value.a : 1;
          valueLayer.characters = rgbToHex(r, g, b, a);
        }
      } else {
        if (valueLayer) valueLayer.characters = JSON.stringify(value);
      }
    } else {
      if (valueLayer) valueLayer.characters = 'N/A';
    }

    // Update swatch
    if (swatchLayer) {
      const resolvedValue = fullVariable.resolveForConsumer(swatchLayer);
      if (resolvedValue && resolvedValue.value) {
        const color = resolvedValue.value as RGB | RGBA;
        swatchLayer.fills = [{
          type: 'SOLID', 
          color: {r: color.r, g: color.g, b: color.b}, 
          opacity: 'a' in color ? color.a : 1
        }];
      } else {
        swatchLayer.fills = [{type: 'SOLID', color: {r: 1, g: 1, b: 1}, opacity: 0}];
      }
    }

    // Add the row to the frame
    tokenFrame.appendChild(rowInstance);
  }

  console.log("Table updated!");
}