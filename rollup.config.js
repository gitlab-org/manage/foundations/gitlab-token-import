const typescript = require('rollup-plugin-typescript2');

module.exports = {
  input: './src/main.ts',
  output: {
    file: 'dist/main.js',
    format: 'iife',
    name: 'GitLabDesignTokens',
  },
  plugins: [typescript()],
  watch: {
    include: ['src/**', '*.html'],
    exclude: ['node_modules/**', 'dist/**'],
  },
};
